﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed;

    [SerializeField] Game game;

    public Animator animator;
    public new Rigidbody2D rigidbody;

     private void FixedUpdate() {
        // -1 / 0 / 1 = Left / Idle / Right
        float input = Input.GetAxisRaw("Horizontal");
        animator.SetFloat("Direction", input);
        // Input movement according to rotation
        Vector2 movement = (rigidbody.rotation == 0) ? new Vector2(input, 0) : new Vector2(0, input * Mathf.Sign(rigidbody.rotation));
        float distance = .35f;
        if (movement == Vector2.down) {
            movement *= 2;
            distance *= 2;
        }
        Vector2 feetDirection = (rigidbody.rotation == 0) ? Vector2.down : new Vector2(Mathf.Sign(rigidbody.rotation), 0);
        // Update position
        rigidbody.position += movement * speed * Time.deltaTime;
        // Check collision on movement direction
        Debug.DrawRay(rigidbody.position - feetDirection * .35f, Vector2.left * distance, Color.green);
        Debug.DrawRay(rigidbody.position - feetDirection * .35f, Vector2.right * distance, Color.green);
        if (movement != Vector2.up && movement != Vector2.zero) {
            RaycastHit2D hit = Physics2D.Raycast(rigidbody.position - feetDirection * .35f, movement, distance);
            if (hit.collider != null) {
                if (hit.collider.name == "Border" || (hit.collider.tag == "Block" && hit.collider.GetComponent<Block>().state != Block.State.Fixed)) {
                    // TODO bug when speed is too high
                    rigidbody.position -= movement * speed * Time.deltaTime;
                    // rigidbody.position = Utils.RoundVector2(rigidbody.position);
                } else if (hit.collider.tag == "Block" || hit.collider.name == "Ground") {
                    rigidbody.rotation = (rigidbody.rotation == 0) ? 90 * input : 0;
                    rigidbody.position = Utils.RoundVector2(rigidbody.position);
                }
            }
        }
        // Check if there is something under the player feets
        Debug.DrawRay(rigidbody.position, feetDirection * .7f, Color.green);
        RaycastHit2D hitFeet = Physics2D.Raycast(rigidbody.position, feetDirection, .7f);
        if (hitFeet.collider == null || hitFeet.collider.tag == "Ball") {
            rigidbody.rotation = (rigidbody.rotation == 0) ? -90 * input : 0;
            rigidbody.position = Utils.RoundVector2(rigidbody.position);
            rigidbody.position += feetDirection * .5f;
        }
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (collider != null && collider.tag == "Ball") {
            game.PickBall(collider.gameObject);
        }
    }
}