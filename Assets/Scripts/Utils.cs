﻿using UnityEngine;

public static class Utils
{
    public static int W = 8;
    // public static int W = 2;
    public static int H = 8;

    public static int MAX_BLOCKS_CAN_SPAWN = 5;
    public static int[] CAN_SPAWN = { 45, 34, 21, 13, 8, 4, 0, 0 };

    public static Vector2 RoundVector2(Vector2 vector) {
        return new Vector2(Mathf.Round(vector.x), Mathf.Round(vector.y));
    }

    public static Vector3 RoundVector3(Vector3 vector) {
        return new Vector3(Mathf.Round(vector.x), Mathf.Round(vector.y), Mathf.Round(vector.z));
    }

    public static void Snap(this Vector2 vector) {
        vector = new Vector2(Mathf.Round(vector.x), Mathf.Round(vector.y));
    }

    public static void Snap(this Vector3 vector) {
        vector = new Vector3(Mathf.Round(vector.x), Mathf.Round(vector.y), Mathf.Round(vector.z));
    }
}

// Raycast ignore yourself https://stackoverflow.com/questions/24563085/raycast-but-ignore-yourself
// Animations https://www.youtube.com/watch?v=Vfq13LRggwk&list=PL4vbr3u7UKWp0iM1WIfRjCDTI03u43Zfu&index=4
// Think Component over Ineritance
// - https://answers.unity.com/questions/119516/inheriting-from-a-class-that-inherits-from-monobeh.html
// - https://gamedevelopment.tutsplus.com/articles/unity-now-youre-thinking-with-components--gamedev-12492
// - https://forum.unity.com/threads/unity-best-architecture-and-design-pattern.424786/
// - https://www.raywenderlich.com/7630142-entity-component-system-for-unity-getting-started

// Corroutine https://www.universityofgames.net/coroutines-in-unity/

// List & Array https://hub.packtpub.com/arrays-lists-dictionaries-unity-3d-game-development/