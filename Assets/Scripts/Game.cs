﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    public GameObject blockPrefab;
    public GameObject ballPrefab;

    [SerializeField] new Camera camera;
    [SerializeField] Player player;
    [SerializeField] Text floorScore;
    [SerializeField] Text ballScore;

    public List<GameObject> blocks = new List<GameObject>();
    public List<GameObject> balls = new List<GameObject>();

    private float timeBeforeNextBlocks;  // seconds
    private float timeBeforeNextBall;  // seconds

    private void Start() {
        timeBeforeNextBall = 3;
    }

    public int GetNumberOfBlocksInColumn(int column) {
        return blocks.FindAll(block => block.transform.position.x == column).Count;
    }

    public int GetNumberOfBlocksInRow(int row) {
        return blocks.FindAll(block => block.transform.position.y == row).Count;
    }

    public GameObject GetBallInColumn(int column) {
        return balls.Find(ball => ball.transform.position.x == column);
    }

    // Instanciate a certains amount of blocks on top of the screen
    private void SpawnBlocks() {
        int number = Random.Range(1, Utils.MAX_BLOCKS_CAN_SPAWN + 1);
        // int number = 1;
        bool[] blockInColumn = new bool[Utils.W];
        for (int i = 0; i < 10000 && number > 0; ++i) {
            int x = Random.Range(0, Utils.W);
            if (!blockInColumn[x] && Random.Range(0, Utils.CAN_SPAWN[0]) < Utils.CAN_SPAWN[GetNumberOfBlocksInColumn(x)]) {
                Vector2 position = new Vector2(x, Utils.H + 2 + transform.position.y - 4.5f);
                GameObject block = Instantiate(blockPrefab, position, Quaternion.identity);
                block.transform.SetParent(camera.transform);
                blocks.Add(block);
                blockInColumn[x] = true;
                number--;
            }
        }
    }

    private void SpawnBall() {
        for (int i = 0; i < 10000; ++i) {
            int x = Random.Range(0, Utils.W);
            if (GetBallInColumn(x) == null) {
                Vector2 position = new Vector2(x, Utils.H + 2);
                GameObject ball = Instantiate(ballPrefab, position, Quaternion.identity);
                ball.transform.SetParent(camera.transform);
                balls.Add(ball);
                int ranType = Random.Range(0, 9);
                int type = (ranType == 0) ? 2 : (ranType < 4) ? 1 : 0;
                ball.GetComponent<Ball>().SetType(type);
                break;
            }
        }
    }

    public void DestroyBall(GameObject ball) {
        balls.Remove(ball);
        Destroy(ball);
        // If there is no balls anymore directly spawn another
        if (balls.Count == 0) {
            timeBeforeNextBall = 1;
        }
    }

    public void PickBall(GameObject ball) {
        ballScore.text = "" + (int.Parse(ballScore.text) + ball.GetComponent<Ball>().GetPoints());
        DestroyBall(ball);
    }

    public void DestroyBlock(GameObject block) {
        blocks.Remove(block);
        Destroy(block);
    }

    private void FixedUpdate() {
        timeBeforeNextBlocks -= Time.deltaTime;
        timeBeforeNextBall -= Time.deltaTime;
        if (timeBeforeNextBlocks < 0) {
            SpawnBlocks();
            // timeBeforeNextBlocks = .5f;
            timeBeforeNextBlocks = 1.5f;
        }
        if (timeBeforeNextBall < 0 && balls.Count < 3) {
            SpawnBall();
            timeBeforeNextBall = Random.Range(3, 6) * balls.Count;
        }
        if (GetNumberOfBlocksInRow(0) == Utils.W) {
            Vector3 target = new Vector3(3.5f, 6.5f, -10f);
            camera.transform.position = Vector3.MoveTowards(camera.transform.position, target, 5 * Time.deltaTime);
            if (camera.transform.position == target) {
                camera.transform.position = new Vector3(3.5f, 5.5f, -10f);
                // Destroy last line game objects
                blocks.FindAll(block => block.transform.position.y == 0).ForEach(block => Destroy(block));
                blocks.RemoveAll(block => block.transform.position.y == 0);
                // Move all fixed blocks down
                blocks.FindAll(block => block.GetComponent<Block>().state == Block.State.Fixed).ForEach(block => block.transform.position += Vector3.down);
                // Move all fixed balls down
                balls.FindAll(ball => ball.GetComponent<Ball>().state == Ball.State.Fixed).ForEach(ball => ball.transform.position += Vector3.down);
                // Move the player down
                player.gameObject.transform.position += Vector3.down;
                // Increase floor score
                floorScore.text = "" + (int.Parse(floorScore.text) + 1);
            }
        }
    }
}