﻿using UnityEngine;

public class Block : MonoBehaviour
{
    // Helpers
    public enum State { Spawning, Falling, Stopping, Fixed }
    
    // Developper variables
    public float speed; // TODO
    public State state = State.Spawning;
    private float hitPointY;

    // Singleton
    private Game game;

    // Unity variables
    public Animator animator;
    public new Rigidbody2D rigidbody;
    public new BoxCollider2D collider;

    // Disable collider to avoid collisions during spawn animation
    private void Start() {
        game = FindObjectOfType<Game>();
    }

    private void FixedUpdate() {
        if (state == State.Spawning) {
            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1) {
                // Set velocity to constant speed falling (over gravityScale acceleration, it's a feature !)
                rigidbody.velocity = Vector2.down * speed;
                // Enable collider to finally perform collisions
                collider.enabled = true;
                state = State.Falling;
            }
        }
        if (state == State.Falling) {
            Vector2 position = rigidbody.position + rigidbody.velocity * Time.deltaTime;
            float distance = .5f + Vector2.Distance(position, rigidbody.position);
            Debug.DrawRay(position, Vector2.down * distance, Color.green);
            // TODO make 2 Raycast, one on each side, to perfectly trigger player touch
            RaycastHit2D hit = Physics2D.Raycast(position, Vector2.down, distance);
            if (hit.collider != null) {
                if (hit.collider.name == "Ground" || (hit.collider.tag == "Block" && hit.collider.gameObject.GetComponent<Block>().state == State.Fixed))  {
                    hitPointY = hit.point.y;
                    state = State.Stopping;
                } else if (hit.collider.name == "Player") {
                    // TODO death mechanics
                    // Debug.Log("DEATH");
                    // game.DestroyBlock(gameObject);
                }
            }
        } else if (state == State.Stopping) {
            rigidbody.velocity = Vector2.zero;
            transform.position = new Vector2(rigidbody.position.x, Mathf.Ceil(hitPointY));
            transform.SetParent(null);
            game.DestroyBall(game.balls.Find(ball => ball.transform.localPosition == transform.localPosition && ball.GetComponent<Ball>().state == Ball.State.Fixed));
            state = State.Fixed;
        }
    }
}