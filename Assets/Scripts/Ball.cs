﻿using UnityEngine;

public class Ball : MonoBehaviour
{
    // Helpers
    public enum State { Falling, Stopping, Fixed }
 
    // Developper variables
    public float speed; // TODO
    public State state = State.Falling;
    private float hitPointY;
    private int type;

    // Singleton
    private Game game;

    // Unity variables
    public Sprite[] sprites;
    public SpriteRenderer spriteRenderer;
    public new Rigidbody2D rigidbody;

    private void Start() {
        game = FindObjectOfType<Game>();
        rigidbody.velocity = Vector2.down * speed;
    }

    public void SetType(int m_type) {
        type = m_type;
        spriteRenderer.sprite = sprites[type];
    }

    public int GetPoints() {
        int[] values = {2, 5, 30};
        return values[type];
    }

    private void FixedUpdate() {
        if (state == State.Falling) {
            Vector2 position = rigidbody.position + rigidbody.velocity * Time.deltaTime;
            float distance = .5f + Vector2.Distance(position, rigidbody.position);
            Debug.DrawRay(position, Vector2.down * distance, Color.green);
            RaycastHit2D hit = Physics2D.Raycast(position, Vector2.down, distance);
            if (hit.collider != null) {
                if (hit.collider.name == "Ground" || (hit.collider.tag == "Block" && hit.collider.gameObject.GetComponent<Block>().state >= Block.State.Stopping))  {
                    hitPointY = hit.point.y;
                    state = State.Stopping;
                }
            }
        } else if (state == State.Stopping) {
            rigidbody.velocity = Vector2.zero;
            transform.position = new Vector2(rigidbody.position.x, Mathf.Ceil(hitPointY));
            transform.SetParent(null);
            state = State.Fixed;
        }
    }
}
